#!/bin/sh

sudo apt remove --purge -qq -y \
  vim \
  vim-common \
  vim-gui-common \
  vim-runtime \
  vim-tiny

sudo apt install -qq -y \
  liblua5.1-dev \
  libluajit-5.1-dev \
  libperl-dev \
  lua5.1 \
  luajit \
  python-dev \
  python3-dev \
  ruby-dev \
  xorg-dev

cd vim/src
make distclean
./configure \
  --enable-cscope \
  --enable-fail-if-missing \
  --enable-gui=auto \
  --enable-largefile \
  --enable-luainterp \
  --enable-multibyte \
  --enable-perlinterp \
  --enable-python3interp \
  --enable-pythoninterp \
  --enable-rubyinterp \
  --prefix=/usr \
  --with-compiledby='Seong Yong-ju <sei40kr@gmail.com>' \
  --with-features=huge \
  --with-lua-prefix=/usr \
  --with-luajit
[ $? -eq 0 ] && make && sudo make install

