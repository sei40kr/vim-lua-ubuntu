# vim-lua-ubuntu
Vim installation with clipboard, Perl, Python (2,3), Ruby, Lua support.
## DEPRECATED
Deprecated: Let's use Neovim instead.
## Installation
Clone this repo and just run a following command.
```bash
git clone --recursive https://sei40kr@gitlab.com/sei40kr/vim-lua-ubuntu.git
cd vim-lua-ubuntu
./install.sh
```
Caution: Current vim will be removed.

